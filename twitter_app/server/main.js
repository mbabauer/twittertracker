import configs from './configs';
import publications from './publications';
import methods from './methods';

configs();
publications();
methods();