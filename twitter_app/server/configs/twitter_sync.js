import _ from 'lodash';
import Twit from 'twit/lib/twitter';
import strsplit from 'strsplit/lib/strsplit';

import {TwitterStatuses, TwitterUsers} from '/lib/collections/collections';
import {TwitterStatus, TwitterUser} from '/lib/collections';

const Twitter = new Twit({
    consumer_key:         'fIdcKzrdTDHsaJnuzaTUXnkHm',
    consumer_secret:      'zJ8oSVLSJHhJZ1y1UYbmZgKJq4YGPNsgmygQ4F5tXyLAwhiRpa',
    access_token:         '52747169-TsNv3vnJ1barujDUE3cTV69487eW2F7PU2VzYsEkC',
    access_token_secret:  'Ob7DJqhU54vrU4Oc7trKb5xSzGVTdwvLtE9BDiVAYnvsa'
});

export default function () {
	console.log('Twitter sync triggered');

	const stream = Twitter.stream('statuses/filter', { track: ['healthcare', 'nasa', 'open source'] });

	stream.on('tweet', Meteor.bindEnvironment(function(tweet) {
		var tweetStatus = TwitterStatuses.findOne({id: tweet.id});
		if (!tweetStatus) {
			console.log('Creating a new status with the _id: ', tweet.id);
			tweetStatus = new TwitterStatus();
		}
		delete tweet.user;	// We need to remove this from the tweet object so the ORM validation doesn't fail
		tweetStatus.set(tweet);

		// Build search_terms
		var terms = [];
		if (tweet.text) {
			terms = terms.concat(strsplit(tweet.text.toLowerCase()));
		}
		if (tweet.entities.urls.display_url) {
			terms = terms.concat(strsplit(tweet.entities.urls.display_url.toLowerCase()));
		}
		terms = _.uniq(terms);
		tweetStatus.set({search_terms: terms});
		
		if (tweet.user) {
			var tweetUser = TwitterUsers.findOne({id: tweet.user.id});
			if (!tweetUser) {
				tweetUser = new TwitterUser();
			}
			tweetUser.save();
			tweetStatus.set({user_id: tweetUser._id});
		}

		tweetStatus.save();
	}));
}