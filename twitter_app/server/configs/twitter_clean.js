import {TwitterStatuses, TwitterUsers} from '/lib/collections/collections';
import {TwitterStatus, TwitterUser} from '/lib/collections';

export default function () {
	Meteor.startup(function () {
		// code to run on server at startup
		var recurrence = new Cron(() => {
			var cleanDate = new Date();
		    cleanDate.setDate(cleanDate.getDate() - 1);
		    var deletedTweetsCount = TwitterStatuses.remove({created_at: {$lt: cleanDate}});
		    if (deletedTweetsCount > 0) {
		    	console.log('Removed ' + deletedTweetsCount + ' Tweets in cleanup routine.');
		    }
		}, {minute: 0});  // Run every hour
	});
}