import twitterSync from './twitter_sync';
import twitterClean from './twitter_clean';

export default function () {
	twitterSync();
	twitterClean();
}