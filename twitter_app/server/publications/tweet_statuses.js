import TwitterStatus from '/lib/collections';
import {TwitterStatuses} from '/lib/collections/collections.js';
import {Meteor} from 'meteor/meteor';
import {check} from 'meteor/check';

export default function () {
    console.log('loading TweetStatus publications');

    Meteor.publish('tweetStatuses.single', function (tweetStatusId) {
        console.log('Returning publication for tweetStatusId ', tweetStatusId);
        const results = TwitterStatuses.find({_id: tweetStatusId});
        console.log('TweetStatus results count: ', results.count());
        return results;
    });

    Meteor.publish('tweetStatuses.limited', function (limit, filters) {
        console.log('Returning publication for TwitterStatuses for with limit of ', limit);
        let findFilter = {};
        if (filters && _.isArray(filters) && filters.length > 0)  {
            findFilter.search_terms = filters;
        }
        const results = TwitterStatuses.find({}, {sort: {timestamp_ms: -1}, limit: limit}); // TODO: Returning all forms here for now...need to only return current user's forms
        console.log('TweetStatus results count: ', results.count());
        return results;
    });
}
