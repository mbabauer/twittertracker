import {Meteor} from 'meteor/meteor'
import {Mongo} from 'meteor/mongo';

export const TwitterStatuses = new Mongo.Collection('twitter_statuses');

export const TwitterUsers = new Mongo.Collection('twitter_users');

if (Meteor.isServer) {
	Meteor.startup(function () {  
		TwitterStatuses._ensureIndex({ "search_terms": 1});
	});	
}
