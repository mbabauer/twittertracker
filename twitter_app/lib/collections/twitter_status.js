import {TwitterStatuses} from './collections.js';

export const TwitterStatus = Astro.Class({
	name: 'TwitterStatus',
	collection: TwitterStatuses,
	fields: {
		created_at: 'date',
		id: 'number',
		id_str: 'string',
		text: 'string',
		source: 'string',
		truncated: 'boolean',
		in_reply_to_status_id: 'number',
		in_reply_to_status_id_str: 'string',
		in_reply_to_user_id: 'number',
		in_reply_to_user_id_str: 'string',
		in_reply_to_screen_name: 'string',
		user_id: 'number',
		geo: 'string',
		coordinates: 'string',
		place: 'string',
		contributors: 'string',
		retweeted_status: 'object',
		is_quote_status: 'boolean',
		retweet_count: 'number',
		favorite_count: 'number',
		entities: 'object',
		extended_entities: 'object',
		favorited: 'boolean',
		retweeted: 'boolean',
		possibly_sensitive: 'boolean',
		filter_level: 'string',
		lang: 'string',
		quoted_status_id: 'number',
		quoted_status_id_str: 'string',
		quoted_status: 'string',
		timestamp_ms: 'number',
		search_terms: 'array'
	},
	relations: {
		user: {
			type: 'one',
			class: 'TwitterUser',
			local: 'user_id',
			foreign: '_id'
		}
	}
});