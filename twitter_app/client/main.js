import {createApp} from 'mantra-core';
import {DocHead} from 'meteor/kadira:dochead';
import initContext from './configs/context';

// modules
import coreModule from './modules/core';
import twitterModule from './modules/twitter';

// init context
const context = initContext();

// create app
const app = createApp(context);
app.loadModule(coreModule);
app.loadModule(twitterModule);
app.init();

// DocHead stuff - This will appear in the <head> section
DocHead.setTitle('WaiveForm');
DocHead.addLink({
 href: 'https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css',
 rel: 'stylesheet'
});
DocHead.addLink({
 href: 'https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap-theme.min.css',
 rel: 'stylesheet'
});