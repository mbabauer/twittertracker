import React from 'react';

import FlowHelpers from '/lib/FlowHelpers'

const Home = () => (
  <div>
    <h1>Mantra</h1>
    <p>
      Welcome to Twitter Tracker 0.0.1
    </p>
    <ul>
      <li>
        Use <a href={FlowHelpers.pathFor('view.ddp')}>Twitter Tracker</a>
      </li>
      <li>
        See <a target="_blank" href="https://bitbucket.org/mbabauer/twittertracker">Source</a>
      </li>
    </ul>
  </div>
);

export default Home;
