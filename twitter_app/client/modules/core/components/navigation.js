import React from 'react';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';

import FlowHelpers from '/lib/FlowHelpers'

const Navigation = () => (
  <Navbar inverse>
    <Navbar.Brand>
      <a href={FlowHelpers.pathFor('home')}>Twitter Tracker</a>
    </Navbar.Brand>

  <Navbar.Collapse>
  <Nav pullRight>
    <NavItem eventKey={1} href={FlowHelpers.pathFor('view.ddp')}>DDP</NavItem>
  </Nav>
  </Navbar.Collapse>
  </Navbar>

);

export default Navigation;
