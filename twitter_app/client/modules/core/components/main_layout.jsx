import React from 'react';
import Navigation from './navigation';
import Footer from './footer';

const Layout = ({content = () => null }) => (
	<div>
	    <Navigation />

	    <div class="container">
	      {content()}
	    </div>

	    <Footer />
	</div>
);

export default Layout;
