export default {
	clearErrors({LocalState}) {
		return LocalState.set('TWEET_PANEL_ERROR', null);
	},

	selectTweetStatus({Meteor, LocalState, FlowRouter}, selectedTweetStatusId) {
		if (!selectedTweetStatusId || selectedTweetStatusId == '') {
			return LocalState.set('TWEET_PANEL_SELECTED_TWEET_ID', undefined);
		} else {
			return LocalState.set('TWEET_PANEL_SELECTED_TWEET_ID', selectedTweetStatusId);
		}
	},

	updateFilters({Meteor, LocalState, FlowRouter}, filters) {
		return LocalState.set('TWEETS_FILTERS', filters);
	}
}
