import {
    useDeps, composeWithTracker, composeAll
} from 'mantra-core';

import DDPPage from '../components/ddp_page.jsx';

export const composer = ({context}, onData) => {
    const {Meteor, Collections, LocalState} = context();

    const selectedTweetStatusId = LocalState.get('TWEET_PANEL_SELECTED_TWEET_ID');
    
    onData(null, {selectedTweetStatusId});
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(DDPPage);