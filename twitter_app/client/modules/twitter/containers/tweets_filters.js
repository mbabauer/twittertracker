import {
    useDeps, composeWithTracker, composeAll
} from 'mantra-core';

import { Meteor } from 'meteor/meteor'

import {TwitterStatuses} from '/lib/collections/collections.js';

import TweetsFilters from '../components/tweets_filters.jsx';

export const composer = ({context, actions}, onData) => {
    const {Meteor, Collections, LocalState} = context();

    const selectedFilters = LocalState.get('TWEETS_FILTERS');
    
    onData(null, {selectedFilters});
};

export const depsMapper = (context, actions) => ({
    updateFilters: actions.tweet_statuses.updateFilters,
    clearErrors: actions.tweet_statuses.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(TweetsFilters);