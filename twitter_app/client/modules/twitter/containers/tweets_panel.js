import {
    useDeps, composeWithTracker, composeAll
} from 'mantra-core';

import { Meteor } from 'meteor/meteor'

import {TwitterStatuses} from '/lib/collections/collections.js';

import TweetsPanel from '../components/tweets_panel.jsx';


export const composer = ({context}, onData) => {
    const {Meteor, Collections, LocalState} = context();

    const selectedFilters = LocalState.get('TWEETS_FILTERS');
    
    if (Meteor.subscribe('tweetStatuses.limited', 300, selectedFilters).ready()) {
        let filter = {};
        if (selectedFilters) {
        	filter.$or = [];
        	_.each(selectedFilters, function(value) {
        		this.push({search_terms: value});
        	}, filter.$or);
        }

        const tweetStatuses = TwitterStatuses.find(filter, {sort: {timestamp_ms: -1}, limit: 10});
        onData(null, {tweetStatuses});
    } else {
    	onData();
    }
};

export const composer2 = ({context}, onData) => {
    const {Meteor, Collections, LocalState} = context();

    const selectedTweetStatusId = LocalState.get('TWEET_PANEL_SELECTED_TWEET_ID');
    
    onData(null, {selectedTweetStatusId});
};

export const depsMapper = (context, actions) => ({
    selectTweetStatus: actions.tweet_statuses.selectTweetStatus,
    clearErrors: actions.tweet_statuses.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    composeWithTracker(composer2),
    useDeps(depsMapper)
)(TweetsPanel);