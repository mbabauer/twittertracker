import {
    useDeps, composeWithTracker, composeAll
} from 'mantra-core';

import RestPage from '../components/rest_page.jsx';

export const composer = ({context}, onData) => {
    const {Meteor, Collections, LocalState} = context();
    
    onData(null, {});
};

export default composeAll(
    composeWithTracker(composer),
    useDeps()
)(RestPage);