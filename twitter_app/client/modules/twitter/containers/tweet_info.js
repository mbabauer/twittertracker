import {
    useDeps, composeWithTracker, composeAll
} from 'mantra-core';

import { Meteor } from 'meteor/meteor'

import {TwitterStatuses} from '/lib/collections/collections.js';

import TweetInfo from '../components/tweet_info.jsx';

export const composer = ({context, tweetId}, onData) => {
    const {Meteor, Collections, LocalState} = context();

    if (Meteor.subscribe('tweetStatuses.single', tweetId).ready()) {
        const tweetStatus = TwitterStatuses.findOne({_id: tweetId});
        onData(null, {tweetStatus, tweetId});
    } else {
    	onData();
    }
};

export const depsMapper = (context, actions) => ({
    selectTweetStatus: actions.tweet_statuses.selectTweetStatus,
    clearErrors: actions.tweet_statuses.clearErrors,
    context: () => context
});

export default composeAll(
    composeWithTracker(composer),
    useDeps(depsMapper)
)(TweetInfo);