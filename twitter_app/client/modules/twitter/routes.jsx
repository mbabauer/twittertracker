import React from 'react';
import {mount} from 'react-mounter';

import MainLayout from '/client/modules/core/components/main_layout.jsx';
import RestPage from './containers/rest_page.js';
import DDPPage from './containers/ddp_page.js';

export default function (injectDeps, {FlowRouter}) {
  const MainLayoutCtx = injectDeps(MainLayout);

  FlowRouter.route('/rest', {
    name: 'view.rest',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<RestPage />)
      });
    }
  });

  FlowRouter.route('/ddp', {
    name: 'view.ddp',
    action() {
      mount(MainLayoutCtx, {
        content: () => (<DDPPage />)
      });
    }
  });
}
