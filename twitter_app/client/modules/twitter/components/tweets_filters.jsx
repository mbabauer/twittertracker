import React from 'react';

import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';

import FormGroup from 'react-bootstrap/lib/FormGroup';
import Checkbox from 'react-bootstrap/lib/Checkbox';

import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';

function isFilterChecked(filterValue, selectedFilters) {
	return (selectedFilters && _.contains(selectedFilters, filterValue));
}

class TweetsPanel extends React.Component {
	constructor(...args) {
		super(...args);

		const {selectedFilters, updateFilters} = this.props;

		if (!selectedFilters) {
			updateFilters(['nasa', 'healthcare', 'open source']);
		}
	}

	render() {
		const {selectedFilters} = this.props;

		const self = this;

		return (
			<FormGroup id="tweet_filters">
				<Checkbox inline checked={isFilterChecked('nasa', selectedFilters)} ref="nasa_filter_btn" type="checkbox" name="nasa" value="nasa" onClick={self.filterChanged.bind(self)}>NASA</Checkbox>
				{' '}
				<Checkbox inline checked={isFilterChecked('healthcare', selectedFilters)} ref="healthcare_filter_btn" type="checkbox" name="healthcare" value="healthcare" onClick={self.filterChanged.bind(self)}>Healthcare</Checkbox>
				{' '}
				<Checkbox inline checked={isFilterChecked('open source', selectedFilters)}ref="opensource_filter_btn" type="checkbox" name="opensource" value="open source" onClick={self.filterChanged.bind(self)}>Open Source</Checkbox>
			</FormGroup>
		);
	}

	filterChanged(event) {
		// Because the test cannot get event argument
	    // so call preventDefault() on undefined cause an error
	    if (event && event.preventDefault) {
			event.preventDefault();
	    }

	    let filterVals = []
	    $('#tweet_filters input:checked').each(function() {
	    	filterVals.push($(this).val());
	    });

	    const {updateFilters} = this.props;
	    updateFilters(filterVals);
	}
}

export default TweetsPanel;