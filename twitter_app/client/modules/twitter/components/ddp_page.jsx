import React from 'react';

import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';

import TweetsFilters from '../containers/tweets_filters';
import TweetsPanel from '../containers/tweets_panel';
import TweetInfo from '../containers/tweet_info';

class DDPPage extends React.Component {
	render() {
		const {selectedTweetStatusId} = this.props;

		return (
			<Grid>
				<Row>
					<Col md={4}>
						<Row>
							<Col md={12}>
								<TweetsFilters />
							</Col>
						</Row>
						<Row>
							<Col md={12}>
								<TweetsPanel tweetId={selectedTweetStatusId} />
							</Col>
						</Row>
					</Col>
					<Col md={8}>
						{(() => {
							if (selectedTweetStatusId) {
								return (<TweetInfo tweetId={selectedTweetStatusId} />);
							} else {
								return "None Selected";
							}
						})()}
					</Col>
				</Row>
			</Grid>
		);
	}
}

export default DDPPage;