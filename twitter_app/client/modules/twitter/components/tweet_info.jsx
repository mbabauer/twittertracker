import React from 'react';

import Panel from 'react-bootstrap/lib/Panel';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';

import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';

import Table from 'react-bootstrap/lib/Table';

import {Chart} from 'react-google-charts';

function isSelected(tweetId, selectedTweetStatusId) {
	return (tweetId && tweetId == selectedTweetStatusId);
}

function title(tweetStatus) {
	if (tweetStatus) {
		return <h3>Tweet ID: <small>{tweetStatus.id}</small></h3>;
	} else {
		return <h3>Tweet</h3>;
	}
}

class TweetInfo extends React.Component {
	render() {
		const {tweetStatus, tweetId} = this.props;

		const self = this;

		return (
			<div>
				<Panel header={title(tweetStatus)}>
					<div>
						<Chart chartType="GeoChart" width={"100%"} height={"300px"} graph_id = "geochart_tweet" data={[['Country', 'Count'],[tweetStatus.lang.toUpperCase(), 1]]} />
					</div>

					<div>
						<Table striped={true}>
							<tbody>
								<tr>
									<th colSpan="2" className="warning">Tweet Info</th>
								</tr>
								<tr>
									<th>Tweet ID</th><td>{tweetStatus.id}</td>
								</tr>
								{(() => {
									if (tweetStatus.user) {
										return (
											<tr>
												<th>User</th><td>{tweetStatus.user.name} ({tweetStatus.user.id})</td>
											</tr>
										);
									}
								})()}
								<tr>
									<th>Language</th><td>{tweetStatus.lang}</td>
								</tr>
								<tr>
									<th>Source</th><td dangerouslySetInnerHTML={{__html: tweetStatus.source}}></td>
								</tr>

								{(() => {
									if (tweetStatus.extended_entities && tweetStatus.extended_entities.media) {
										return (
											<tr>
												<th>Attached Media</th>
												<td>
													{tweetStatus.extended_entities.media.map(function(media, index) {
														<div dangerouslySetInnerHTML={{__html: media.media_url}}></div>
													})}
												</td>
											</tr>
										);
									}
								})()}

								{(() => {
									if (tweetStatus.retweeted_status) {
										const retweetStatus = tweetStatus.retweeted_status;

										return [
											<tr key="retweetStatus_1">
												<th colSpan="2" className="warning">Retweet Info</th>
											</tr>,
											<tr key="retweetStatus_2">
												<th>Tweet ID</th><td>{retweetStatus.id}</td>
											</tr>,
											<tr key="retweetStatus_3">
												<th>Text</th><td>{retweetStatus.text}</td>
											</tr>,
											<tr key="retweetStatus_4">
												<th>User</th><td>{retweetStatus.user.name} ({retweetStatus.user.id})</td>
											</tr>,
											<tr key="retweetStatus_5">
												<th>Language</th><td>{retweetStatus.lang}</td>
											</tr>,
											<tr key="retweetStatus_6">
												<th>Source</th><td dangerouslySetInnerHTML={{__html: retweetStatus.source}}></td>
											</tr>
										];
									}
								})()}
							</tbody>
						</Table>
					</div>
				</Panel>
			</div>
		);
	}
}

export default TweetInfo;