import React from 'react';

import ListGroup from 'react-bootstrap/lib/ListGroup';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';

import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';

function isSelected(tweetId, selectedTweetStatusId) {
	return (tweetId && tweetId == selectedTweetStatusId);
}

class TweetsPanel extends React.Component {
	render() {
		const {tweetStatuses, selectedTweetStatusId} = this.props;

		const self = this;

		return (
			<ListGroup cssClass="tweetsPanel">
				{tweetStatuses.map(function(tweet, index){
					return <ListGroupItem key={tweet._id} id={tweet._id} onClick={self.tweetSelected.bind(self)} active={isSelected(tweet._id, selectedTweetStatusId)}>{tweet.text}</ListGroupItem>;
				})}
			</ListGroup>
		);
	}

	tweetSelected(event) {
		// Because the test cannot get event argument
	    // so call preventDefault() on undefined cause an error
	    if (event && event.preventDefault) {
			event.preventDefault();
	    }

	    const {selectTweetStatus, selectedTweetStatusId} = this.props;
	    const tweetId = event.target.id;

	    if (tweetId && tweetId != selectedTweetStatusId) {
	    	selectTweetStatus(tweetId);
	    } else {
	    	selectTweetStatus(undefined);
	    }
	}
}

export default TweetsPanel;